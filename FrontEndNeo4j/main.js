import { Comic } from "./comic.js";
import { User } from "./user.js";
import { Rated } from "./rated.js";

let loggedIn = false;

var home = document.body.querySelector(".logo"); //za logo napravi homepage
home.onclick = (event) => {
    let mainContainer = document.querySelector(".mainContainer")
    while(mainContainer.firstChild) {
        mainContainer.removeChild(mainContainer.lastChild)
    }

    let mainHomeContainer = document.createElement('div')
    mainHomeContainer.classList = 'mainHomeContainer'

    let myHomepageLabel = document.createElement('label')
    myHomepageLabel.className = "titleLabel1"
    myHomepageLabel.textContent = "Home page"
    mainHomeContainer.appendChild(myHomepageLabel)

    let homeContainer = document.createElement('div')
    homeContainer.classList = 'homeContainer' 
    mainHomeContainer.appendChild(homeContainer)

    mainContainer.appendChild(mainHomeContainer)

    let popularContainer = document.createElement('div')
    popularContainer.className = "popularContainer"
    homeContainer.appendChild(popularContainer)

    let labelPopular = document.createElement('label')
    labelPopular.className = "pageTitleLabel"
    labelPopular.textContent = "Popular Comics:"
    popularContainer.appendChild(labelPopular)

    let friendContainer = document.createElement('div')
    friendContainer.className = "friendContainer"
    homeContainer.appendChild(friendContainer) 

    let labelFriend = document.createElement('label')
    labelFriend.className = "pageTitleLabel"
    labelFriend.textContent = "Friend Activity:"
    friendContainer.appendChild(labelFriend)

    let recContainer = document.createElement('div')
    recContainer.className = "recContainer"
    homeContainer.appendChild(recContainer)

    let labelrecomendations = document.createElement('label')
    labelrecomendations.className = "pageTitleLabel"
    labelrecomendations.textContent = "Recomendations:"
    recContainer.appendChild(labelrecomendations)

    let recFriendContainer = document.createElement('div')
    recFriendContainer.className = "recFriendContainer"
    recContainer.appendChild(recFriendContainer)

    let recByFriendsLabel = document.createElement('label')
    recByFriendsLabel.className = 'emptyScreenLabel1'
    recByFriendsLabel.textContent = 'By your friends\' comics:'
    recFriendContainer.appendChild(recByFriendsLabel)

    let recComicsContainer = document.createElement('div')
    recComicsContainer.className = "recComicsContainer"
    recContainer.appendChild(recFriendContainer)
    
    let labelRecByComics = document.createElement('label')
    labelRecByComics.className = 'emptyScreenLabel1'
    labelRecByComics.textContent = 'By your other comics:'
    recComicsContainer.appendChild(labelRecByComics)

    recContainer.appendChild(recComicsContainer)

    let recSelectedContainer = document.createElement('div')
    recSelectedContainer.className = "recSelectedContainer"
    let labelRecBySelection = document.createElement('label')
    labelRecBySelection.className = 'emptyScreenLabel1'
    labelRecBySelection.textContent = 'By your selected tags:'
    recSelectedContainer.appendChild(labelRecBySelection)
    recContainer.appendChild(recSelectedContainer)

    let recommendedFriendsContainer = document.createElement('div')
    recommendedFriendsContainer.className = 'recommendedFriendContainer'
    popularContainer.appendChild(recommendedFriendsContainer)

    let labelForFriends = document.createElement('label')
    labelForFriends.className = 'emptyScreenLabel1'
    labelForFriends.textContent = 'Recommended friends'
    recommendedFriendsContainer.appendChild(labelForFriends)


    fetch("https://localhost:7012/Comic/GetPopularComics").then(p => {
        p.json().then(data => {
            data.forEach(element => {
                let item = new  Comic(element.comicId, element.title, element.description, element.rating, element.type);
                item.drawComic(popularContainer);
            });
        });
     });
     
    if (!(sessionStorage.getItem("username") === null))
    {
        fetch("https://localhost:7012/Review/GetFriendActivity/" + window.sessionStorage.getItem('username')).then(p => {
            p.json().then(data => {
                data.forEach(element => {
                    let item = new Rated(new User(element.user.name, element.user.username, element.user.password, element.user.email), new Comic(element.comic.comicId, element.comic.title, element.comic.description, element.comic.rating, element.comic.type), element.stars, element.comment, element.dateTime);
                    item.drawReviewWithComicAndUser(friendContainer);
                });
            });
        });
    }
    else
    {
        let label2 = document.createElement('label');
        label2.className = "emptyScreenLabel";
        label2.innerHTML = "You are not logged in. Log in at Profile tab or register at Register tab.";
        friendContainer.appendChild(label2);
    }

    if (!(sessionStorage.getItem("username") === null)) {
        fetch("https://localhost:7012/User/GetFriendRecommendations/" + sessionStorage.getItem("username")).then(p => {
            p.json().then(data => {
                data.forEach(element => {
                    let possibleFriend = new User(element.name, element.username, "", element.email)
                    possibleFriend.drawFriend(recommendedFriendsContainer)
                })
            })
        })
    }
    
    if (!(sessionStorage.getItem("username") === null))
    {
        fetch("https://localhost:7012/Comic/GetRecommendationsByYourComics/" + sessionStorage.getItem('username')).then(p => {
            p.json().then(data => {
                data.forEach(element => {
                    let item = new  Comic(element.comicId, element.title, element.description, element.rating, element.type);
                    item.drawComic(recFriendContainer);
                });
            });
        });
    }
    if (!(sessionStorage.getItem("username") === null))
    {
        fetch("https://localhost:7012/Comic/GetRecommendationsByFriends/" + sessionStorage.getItem('username')).then(p => {
            p.json().then(data => {
                data.forEach(element => {
                    let item = new  Comic(element.comicId, element.title, element.description, element.rating, element.type);
                    item.drawComic(recComicsContainer);
                });
            });
        });
    }


    if (!(sessionStorage.getItem("username") === null))
    {
        fetch("https://localhost:7012/Comic/GetRecommendationsBySelectedTags/" + sessionStorage.getItem('username')).then(p => {
            p.json().then(data => {
                data.forEach(element => {
                    let item = new  Comic(element.comicId, element.title, element.description, element.rating, element.type);
                    item.drawComic(recSelectedContainer);
                });
            });
        });
    }
}

var myComics = document.body.querySelector(".myComics");
myComics.addEventListener('click', (event) => {
    
    let mainContainer = document.querySelector(".mainContainer")
    while(mainContainer.firstChild) {
        mainContainer.removeChild(mainContainer.lastChild)
    }

    let myComicsContainer = document.createElement('div')
    myComicsContainer.classList = "myComicsContainer"
    mainContainer.appendChild(myComicsContainer)

    let myComicsLabel = document.createElement('label')
    myComicsLabel.className = "titleLabel"
    myComicsLabel.textContent = "My Comics"
    myComicsContainer.appendChild(myComicsLabel)
    if (!(sessionStorage.getItem("username") === null))
    {
        fetch("https://localhost:7012/Review/GetMyReviews/" + window.sessionStorage.getItem('username')).then(p => {
            p.json().then(data => {
                data.forEach(element => {
                    let item = new Rated(new User(element.user.name, element.user.username, element.user.password, element.user.email), new Comic(element.comic.comicId, element.comic.title, element.comic.description, element.comic.rating, element.comic.type), element.stars, element.comment, element.dateTime);
                    item.drawReviewWithComic(myComicsContainer);
                });
            });
        });
    }
    else
    {
        let label = document.createElement('label');
        label.className = "emptyScreenLabel";
        label.innerHTML = "You are not logged in. Log in at Profile tab or register at Register tab.";
        myComicsContainer.appendChild(label);
    }
});

var search = document.body.querySelector(".search");
search.addEventListener('click', (event) => {
    //remove everything from main container
    let mainContainer = document.querySelector(".mainContainer")
    while(mainContainer.firstChild) {
        mainContainer.removeChild(mainContainer.lastChild)
    }
    
    let searchContainer = document.createElement('div')
    searchContainer.className = "searchContainer"
    mainContainer.appendChild(searchContainer)

    let searchSidebar = document.createElement('div')
    searchSidebar.className = "searchSidebar"
    searchContainer.appendChild(searchSidebar)

    let resultsContainer = document.createElement('div')
    resultsContainer.className = "resultsContainer"
    searchContainer.appendChild(resultsContainer)


    //dodaj u sidebar kriterijume za pretragu i search dugme kad klikne ispadaju rezultati
    let label = document.createElement('h2');
    label.innerHTML = "Search criteria: ";
    searchSidebar.appendChild(label);

    label = document.createElement('label');
    label.innerHTML = "Title: ";
    searchSidebar.appendChild(label);

    let inputTitle = document.createElement('input');
    inputTitle.type = "text";
    searchSidebar.appendChild(inputTitle);

    label = document.createElement('label');
    label.innerHTML = "Tags: "
    let inputTags = document.createElement('input');
    inputTags.type = 'text';
    searchSidebar.appendChild(label);
    searchSidebar.appendChild(inputTags);

    let dugme = document.createElement('button');
    dugme.innerHTML = "Search";
    searchSidebar.appendChild(dugme);


    dugme.onclick = (event) => {
        let title = document.createElement('h1')
        title.innerHTML = "Search results for '"+ inputTitle.value +"':"
        resultsContainer.appendChild(title)
        let titleList = inputTitle.value.split(' ');
        let titleString = titleList[0];
        titleList.forEach((word, index) => {
            if (index < 1) return;
            titleString = titleString + '+' + word;
        })
        fetch("https://localhost:7012/Comic/Search/" + titleString, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept": "*/*"
            },
            body: JSON.stringify(inputTags.value)
        }).then(request => {
            if (request.ok) {
                request.json().then(data => {
                    data.forEach(element => {
                        let item = new Comic(element.comicId, element.title, element.description, element.rating, element.type);
                        item.drawComic(resultsContainer);
                    })
                })
            }
            else {
                alert ("Bad request");
            }
        })
    }


});


var profile = document.body.querySelector(".profile");
profile.addEventListener('click', (event) => {

    let mainContainer = document.querySelector(".mainContainer")
    while(mainContainer.firstChild) {
        mainContainer.removeChild(mainContainer.lastChild)
    }
    let profileContainer = document.createElement("div")
    profileContainer.className = "profileContainer"
    

    let profileInfoContainer = document.createElement("div")
    profileInfoContainer.className = "profileInfoContainer"
    let friendsContainer = document.createElement("div")
    friendsContainer.className = "friendsContainer"
    profileContainer.appendChild(profileInfoContainer)
    profileContainer.appendChild(friendsContainer)

    if((sessionStorage.getItem("username") === null) && loggedIn) {
        loggedIn = false
    }

    if (!loggedIn && (sessionStorage.getItem("username") === null))
    {
        User.drawLoginForm(profileInfoContainer);
        loggedIn = true;
        mainContainer.appendChild(profileContainer)
    }
    else {
        let x = new User(sessionStorage.getItem('name'), sessionStorage.getItem('username'), sessionStorage.getItem('password'), sessionStorage.getItem('email'));
        fetch("https://localhost:7012/Tag/GetUserTags/" + x.username)
        .then(response => {
            if(response.ok) {
                response.json().then(data => {
                    data.forEach(element => {
                        x.addTag(element.genre);
                    })
                })
                console.log("finished fetching tags");
            }
            else alert("Problem loading tags!")
        })
        loggedIn = true;
        x.crtajFormu(profileInfoContainer);
        mainContainer.appendChild(profileContainer)
    }
});


var register = document.body.querySelector(".register");
register.addEventListener('click', (event) => {

    let mainContainer = document.querySelector(".mainContainer");
    while (mainContainer.firstChild) {
        mainContainer.removeChild(mainContainer.lastChild);
    }

    let profileContainer = document.createElement('div');
    profileContainer.className = 'registerContainer';
    mainContainer.appendChild(profileContainer);

    if ((sessionStorage.getItem("username") !== null)) {
        let label = document.createElement('label')
        label.textContent = "Please log out at Profile tab if you want to create a new account."
        label.className = 'pageTitleLabel'
        alert("You are already logged in!")
        profileContainer.appendChild(label)
        return
    }
    
    
    

    let labela = null;
    let stavka = null;
    let listLabels = ["Name: ", "Username: ", "Email: ", "Password: "];
    let listTypes = ["text", "text", "text", "text"];
    let listClasses = ["nameInput", "usernameInput", "emailInput", "passwordInput"];

    listLabels.forEach((item, index) => {
        labela = document.createElement("label");
        labela.innerHTML = item;
        profileContainer.appendChild(labela);

        stavka = document.createElement("input");
        stavka.type = listTypes[index];
        stavka.className = listClasses[index];
        
        profileContainer.appendChild(stavka);
    })
    let div = document.createElement('div');
    div.classList.add('tagRows');
    let row;
    
    let tagList = [];
    fetch("https://localhost:7012/Tag/GetAllTags").then(request => {
        if (request.ok) {
            request.json().then(data => {
                data.forEach((element, index) => {
                    //crtaj dugme za svaki tag, sa inner html tag.genre, u vise linija
                    (function () {
                        if (index % 15 === 0) {
                            row = document.createElement("div");
                            row.classList.add('tagRow');
                            div.appendChild(row);
                        }
                        let tag = document.createElement('button');
                        tag.className = 'tagButton';
                        tag.innerHTML = element.genre;
                        tag.onclick = () => {
                            //add to user.tags, ali ne mogu u user.tags, tako da bi trebalo da ima neku listu ovde negde
                            if (!tagList.includes(tag.innerHTML)) {
                                tagList.push(tag.innerHTML);
                                tag.classList.add('selected');
                            }
                            
                            else {
                                let index = tagList.indexOf(tag.innerHTML);
                                tagList.splice(index, 1);
                                tag.classList.remove('selected');
                            }
                        }
                        row.appendChild(tag);
                    })();
                })
            })
        }
    })
    profileContainer.appendChild(div);
    let nameIn = profileContainer.querySelector(".nameInput");
    let usernameIn = profileContainer.querySelector(".usernameInput");
    let passwordIn = profileContainer.querySelector(".passwordInput");
    let emailIn = profileContainer.querySelector(".emailInput");

    let dugme = document.createElement('button');
    dugme.innerHTML = "Register";
    dugme.className = "register";
    dugme.onclick = (event) => {
        fetch("https://localhost:7012/User/Register", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept": "*/*"
            },
            body: JSON.stringify({
                name: nameIn.value,
                username: usernameIn.value,
                password: passwordIn.value,
                email: emailIn.value,
                tags: tagList
            })
        }).then(data => {
            if (data.ok) {
                alert("You are registered now! Go to Profile tab to log in.")
            }
            else {
                alert("Your data is invalid. Revise and try to register again!")
            }
        })
    }
    profileContainer.appendChild(dugme);
});

home.onclick(new Event("evenet"))