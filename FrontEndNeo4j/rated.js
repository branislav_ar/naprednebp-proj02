import {Comic} from "./comic.js"
import {User} from "./user.js"

export class Rated {
    constructor(user, comic, stars, review,dateTime)
    {
        this.user = user
        this.comic = comic
        this.stars = stars
        this.comment = review
        this.dateTime = dateTime
        this.container = null
    }

    drawReviewWithComic(host) {
        
        let reviewContainer = document.createElement('div')
        reviewContainer.className = "reviewContainer"

        let reviewHeader = document.createElement('reviewHeader')
        reviewHeader.className = "reviewHeader"

        reviewContainer.appendChild(reviewHeader)

        let reviewHeaderText = document.createElement('label')
        reviewHeaderText.className = "reviewHeaderText"

        reviewHeaderText.textContent = this.comic.title + " - " + this.comic.type
        reviewHeader.appendChild(reviewHeaderText)

        let ratingLabel = document.createElement('label')
        ratingLabel.className = "reviewRatingText"
        ratingLabel.textContent = "Your rating: " + this.stars
        reviewHeader.appendChild(ratingLabel)

        let reviewParagraph = document.createElement("p")
        reviewParagraph.className = "review"
        reviewParagraph.textContent = 'You wrote: "' + this.comment + '"'
        reviewContainer.appendChild(reviewParagraph)

        let reviewFooter = document.createElement('div')
        reviewFooter.className = "reviewFooter"
        reviewContainer.appendChild(reviewFooter)

        reviewHeaderText.onclick = (event) => {
            this.comic.drawComicForm()
        }

        
        host.appendChild(reviewContainer)
    }

    drawReviewWithUser(host) {
        let reviewContainer = document.createElement('div')
        reviewContainer.className = "reviewContainer"
        reviewContainer.classList.add('alignThird')
        let reviewHeader = document.createElement('reviewHeader')
        reviewHeader.className = "reviewHeader"

        reviewContainer.appendChild(reviewHeader)

        let reviewHeaderText = document.createElement('label')
        reviewHeaderText.className = "reviewHeaderText"

        reviewHeaderText.textContent = this.user.username
        reviewHeader.appendChild(reviewHeaderText)

        let starsLabel = document.createElement('label')
        starsLabel.className = "reviewRatingText"
        starsLabel.textContent = "Rated: " + this.stars
        reviewHeader.appendChild(starsLabel)

        let reviewParagraph = document.createElement("p")
        reviewParagraph.className = "review"
        reviewParagraph.textContent = this.user.name + ' wrote: "' + this.comment + '"'
        reviewContainer.appendChild(reviewParagraph)

        let reviewFooter = document.createElement('div')
        reviewFooter.className = "reviewFooter"
        reviewContainer.appendChild(reviewFooter)

        reviewHeaderText.onclick = (event) => {
            this.user.drawUserForm()
        }

        
        host.appendChild(reviewContainer)
    }

    drawReviewWithComicAndUser(host) {
        let reviewContainer = document.createElement('div')
        reviewContainer.className = "reviewContainer"

        let reviewHeader = document.createElement('div')
        reviewHeader.className = "reviewHeader"

        reviewContainer.appendChild(reviewHeader)

        let reviewHeaderText = document.createElement('label')
        reviewHeaderText.className = "reviewHeaderText"

        reviewHeaderText.textContent = this.user.username
        reviewHeader.appendChild(reviewHeaderText)

        let starsLabel = document.createElement('label')
        starsLabel.className = "reviewRatingText"
        starsLabel.textContent = "Rated: " + this.comic.rating
        reviewHeader.appendChild(starsLabel)

        let reviewHeaderComic = document.createElement('div')
        reviewHeaderComic.className = "reviewHeader"

        reviewContainer.appendChild(reviewHeaderComic)

        let reviewHeaderTextComic = document.createElement('label')
        reviewHeaderTextComic.className = "reviewHeaderText"

        reviewHeaderTextComic.textContent = this.comic.title + " - " + this.comic.type
        reviewHeaderComic.appendChild(reviewHeaderTextComic)
        
        let ratingLabel = document.createElement('label')
        ratingLabel.className = "reviewRatingText"
        ratingLabel.textContent = "Your rating: " + this.stars
        reviewHeaderComic.appendChild(ratingLabel)


        let reviewParagraph = document.createElement("p")
        reviewParagraph.className = "review"
        reviewParagraph.textContent = this.user.name + ' wrote: "' + this.comment + '"'
        reviewContainer.appendChild(reviewParagraph)

        let reviewFooter = document.createElement('div')
        reviewFooter.className = "reviewFooter"
        reviewContainer.appendChild(reviewFooter)

        reviewHeaderText.onclick = (event) => {
            this.user.drawUserForm()
        }

        reviewHeaderTextComic.onclick = (event) => {
            this.comic.drawComicForm()
        }

        
        host.appendChild(reviewContainer)
    }

    drawAddReview(host) {
        let reviewContainer = document.createElement('div')
        reviewContainer.className = "reviewContainer"

        let reviewHeader = document.createElement('reviewHeader')
        reviewHeader.className = "reviewHeader"

        reviewContainer.classList.add('colorRed')
        reviewContainer.classList.add('alignSecond')
        
        reviewContainer.appendChild(reviewHeader)

        let reviewHeaderText = document.createElement('label')
        reviewHeaderText.className = "reviewHeaderText"

        reviewHeaderText.textContent = "Your review:"
        reviewHeader.appendChild(reviewHeaderText)

        let starsLabel = document.createElement('label')
        starsLabel.className = "reviewRatingText"
        starsLabel.textContent = "Rated: "
        reviewHeader.appendChild(starsLabel)
        
        let starsInput = document.createElement('select')
        for (let i = 1; i <= 5; i++) {
            let selectOption = document.createElement('option')
            selectOption.innerText = i
            starsInput.appendChild(selectOption)
        }
        starsInput.selectedIndex = this.stars - 1
        reviewHeader.appendChild(starsInput)
        let reviewParagraph = document.createElement("textarea")
        reviewParagraph.className = "review"
        reviewParagraph.textContent = this.comment
        reviewContainer.appendChild(reviewParagraph)

        let reviewFooter = document.createElement('div')
        reviewFooter.className = "reviewFooter"
        reviewContainer.appendChild(reviewFooter)


        
        let addReviewButton = document.createElement('button')
        addReviewButton.textContent = 'Submit Review'
        reviewFooter.appendChild(addReviewButton)

        let deleteReviewButton = document.createElement('button')
        deleteReviewButton.textContent = 'Delete Review'

        if (this.comment !== "")  {
            reviewFooter.appendChild(deleteReviewButton)
        }

        deleteReviewButton.onclick = () => {
            fetch("https://localhost:7012/Review/DeleteReview/" + sessionStorage.getItem('username') + '/' + this.comic.comicId, {
                method: "DELETE"
            }).then(result => {
                if (result.ok) {
                    starsInput.selectedIndex = 0
                    reviewParagraph.value = ""
                    this.comment = ""
                    this.stars = 1
                    reviewFooter.removeChild(deleteReviewButton)
                }
                else {
                    alert('Something went wrong while deleting your review!')
                }
            })
        }


        addReviewButton.onclick = (event) => {
            //zove fetch funkciju
            if (reviewParagraph.value == "") {
                alert("Please write a comment about your experience readig this comic!")
                console.log(JSON.stringify(this))
                return
            }
            if (this.comment == "") {
                this.comment = reviewParagraph.value
                this.stars = starsInput.selectedIndex + 1
                this.dateTime = "2022-03-29T15:56:04"
                fetch("https://localhost:7012/Review/AddReview", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "*/*"
                    },
                    body: JSON.stringify(this)
                }).then(request => {
                    if (request.ok) {
                        alert("Review successfully added!")
                    }
                    else {
                        alert ("Bad request");
                    }
                })
            }
            else {
                this.comment = reviewParagraph.value
                this.stars = starsInput.selectedIndex + 1
                this.dateTime = "2022-03-29T15:56:04"
                fetch("https://localhost:7012/Review/UpdateReview", {
                    method: "PUT",
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "*/*"
                    },
                    body: JSON.stringify(this)
                }).then(request => {
                    if (request.ok) {
                        alert("Review successfully edited!")
                    }
                    else {
                        alert ("Bad request");
                    }
                })
            }
            
        }

        host.appendChild(reviewContainer)
    }
}