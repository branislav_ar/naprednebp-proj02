export class Tag {
    constructor(genre) {
        this.genre = genre;
        this.container = null
    }

    createTagButton(host, tagList)
    {
        (()=> {
            let tag = document.createElement('button');
            tag.className = 'tagButton';
            tag.innerHTML = this.genre;
            console.log(tagList)
            console.log(this)
            console.log(tagList.some((element) =>  element.genre === this.genre ))
            if (tagList.some((element) =>  element.genre === this.genre )) {
                tag.classList.add('selected');
                console.log("hey i am selected " + tag.innerHTML);
            }
            tag.onclick = () => { 
                //add to user.tags, ali ne mogu u user.tags, tako da bi trebalo da ima neku listu ovde negde
                if (!tag.classList.contains('selected')) {
                    tagList.push(this);
                    tag.classList.add('selected');
                    console.log(tagList)
                }
                else {
                    let index = tagList.indexOf(this);
                    tagList.splice(index, 1);
                    tag.classList.remove('selected');
                    console.log(tagList)
                }
            }
            host.appendChild(tag)
        })();
    }
}