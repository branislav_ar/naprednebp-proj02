import {Rated} from "./rated.js"
import {User} from "./user.js"

export class Comic {
    constructor(comicId,tit, desc, rat, tip) {
        this.comicId = comicId
        this.title = tit;
        this.description = desc;
        this.rating = rat;
        this.type = tip;
        this.kontejner = null;
    }

    drawComic(host) {
        if (!host) {
            throw new Exception("Roditeljski element ne postoji");
        }
        
        this.kontejner = document.createElement("div");
        this.kontejner.classList.add("containerForComic");
        
        host.appendChild(this.kontejner);

        var kontForma = document.createElement("div");
        kontForma.className = "kontForma";
        this.kontejner.appendChild(kontForma);

        var labela = document.createElement("label");
        labela.innerHTML = "Title: " + this.title;
        kontForma.appendChild(labela);
        labela = document.createElement("label");
        labela.innerHTML = "Rating: " + this.rating;
        kontForma.appendChild(labela);

        kontForma.addEventListener('click', (event) => {
            this.drawComicForm();
        })
        
    }

    drawComicForm() {
        let mainContainer = document.querySelector(".mainContainer")
        while(mainContainer.firstChild) {
            mainContainer.removeChild(mainContainer.lastChild)
        }

        //sad imam cist main container, vreme je za crtanje comic-a
        let comicForm = document.createElement('div')
        comicForm.className = '.comicForm'

        let comicFormHeader = document.createElement('div')
        comicFormHeader.className = 'comicFormHeader'
        comicForm.appendChild(comicFormHeader)

        let comicTitleLabel = document.createElement('label')
        comicTitleLabel.className = 'comicFormHeaderText'
        comicTitleLabel.textContent = this.title
        comicFormHeader.appendChild(comicTitleLabel)

        let comicRatingAverage = document.createElement('label')
        comicRatingAverage.className = 'comicHeaderRating'
        comicRatingAverage.textContent = "Rating: " + this.rating
        comicFormHeader.appendChild(comicRatingAverage)

        let comicFormBody = document.createElement('div')
        comicFormBody.className = "comicFormBody"
        comicForm.appendChild(comicFormBody)

        let comicFormComicInfo = document.createElement('div')
        comicFormComicInfo.className = 'comicFormComicInfo'
        comicFormBody.appendChild(comicFormComicInfo)

        let typeLabel = document.createElement('label')
        typeLabel.textContent = "type: " + this.type
        comicFormComicInfo.appendChild(typeLabel)

        let descriptionLabel = document.createElement('p')
        descriptionLabel.textContent = "Description: " + this.description
        comicFormComicInfo.appendChild(descriptionLabel)

        let comicFormReviews = document.createElement('div')
        comicFormReviews.className = 'comicFormReviews'
        comicFormBody.appendChild(comicFormReviews)

        let comicFormReviewLabel = document.createElement('label')
        comicFormReviewLabel.textContent = "Reviews:"
        comicFormReviewLabel.classList.add('alignToStart')
        comicFormReviews.appendChild(comicFormReviewLabel)

        //pokupi info iz comic reviews za ove, crtaj sa user-om

        fetch("https://localhost:7012/Review/GetComicReviews/" + this.comicId).then(p => {
            let spawnedReviewEntryForm = false
            p.json().then(data => {
                //var list = []
                data.forEach(element => {
                    let item = new Rated(new User(element.user.name, element.user.username, element.user.password, element.user.email), new Comic(element.comic.comicId, element.comic.title, element.comic.description, element.comic.rating, element.comic.type), element.stars, element.comment, element.dateTime);
                    if (item.user.username == sessionStorage.getItem('username')) {
                        item.drawAddReview(comicFormReviews)
                        spawnedReviewEntryForm = true
                    } else {
                        item.drawReviewWithUser(comicFormReviews);
                    }
                    
                });
                if (!spawnedReviewEntryForm) {
                    let emptyReviewEntryForm = new Rated(new User(sessionStorage.getItem('name'), sessionStorage.getItem('username'), "", sessionStorage.getItem('email')), new Comic(this.comicId, this.title, this.description, this.rating, this.type), 1, "", "")
                    emptyReviewEntryForm.drawAddReview(comicFormReviews)
                    spawnedReviewEntryForm = true
                }
            });
        });
        //let emptyItem = new Rated(new User("", sessionStorage.getItem('username'), "", ""), new Comic(this.comicId, this.title, this.description, this.rating, this.type), 0, "", ""))
        let comicFormFooter = document.createElement('div')
        comicFormFooter.className = 'comicFormFooter'
        let label = document.createElement('label')
        label.className = 'emptyScreenLabel'
        label.textContent = "If you like this comic, you might like:"
        comicFormFooter.appendChild(label)
        let comicContainer = document.createElement('div')
        comicContainer.className = 'rowComicContainer'
        comicFormFooter.appendChild(comicContainer)
        fetch("https://localhost:7012/Comic/SimilarToComic/" + this.comicId).then(p => {
            if(p.ok) {
                p.json().then(data => {
                    data.forEach(element => {
                        let comic = new Comic(element.comicId, element.title, element.description, element.rating, element.type)
                        comic.drawComic(comicContainer)
                    })
                })
            }
            else {
                alert("Problem fetching data. Check your backend and database service.")
            }
        })

        comicForm.appendChild(comicFormFooter)
        mainContainer.appendChild(comicForm)
        
    }
}