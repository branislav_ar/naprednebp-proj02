import { Tag } from "./tag.js";
import {Rated} from "./rated.js"
import {Comic} from "./comic.js"

export class User {
    constructor(name, username, password, email) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.email = email;
        this.friends = [];
        this.tags = [];
    }

    addTag(tag) {
        this.tags.push(new Tag(tag));
    }

    static drawLoginForm(host) {
        if (!host) {
            throw new Exception("Roditeljski element ne postoji");
        }
        let user;
        while (host.firstChild !== null) {
            host.removeChild(host.lastChild);
        }

        let divItem = document.createElement('div');
        divItem.className = "inputElement";

        let usernameLabel = document.createElement("label");
        usernameLabel.className = "usernameLabel";
        usernameLabel.innerHTML = "Username: ";
        let inputUsername = document.createElement('input');
        inputUsername.className = "inputUsername";
        divItem.appendChild(usernameLabel);
        divItem.appendChild(inputUsername);
        host.appendChild(divItem);


        let newDiv = document.createElement('div')
        newDiv.className = "inputElement"
        let passwordLabel = document.createElement("label");
        passwordLabel.className = "passwordLabel";
        passwordLabel.innerHTML = "Password: ";
        let inputPassword = document.createElement('input');
        inputPassword.className = "inputPassword";
        newDiv.appendChild(passwordLabel);
        newDiv.appendChild(inputPassword);
        host.appendChild(newDiv);
        

        let dugme = document.createElement('button');
        dugme.className = "logInBtn";
        dugme.innerHTML = "Log in";
        host.appendChild(dugme);
        
        dugme.onclick = (event) => {
            fetch("https://localhost:7012/User/Login/" + inputUsername.value, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "*/*"
                },
                body: JSON.stringify(inputPassword.value)
            }).then(response => {
                    if(response.ok) {
                        response.json().then(x => {
                            user = new User(x.name, x.username, x.password, x.email);
                            User.saveToLocalStorage(x.name, x.username, x.password, x.email);
                            x.tags.forEach(tag => {
                                user.addTag(tag.genre);
                                console.log(tag.genre)
                            })
                            user.crtajFormu(host);
                        })
                    }
                    else
                    {
                        alert("Login information is not correct");
                    }
            })
        }
    }

    static saveToLocalStorage(name, username, password, email) {
        window.sessionStorage.setItem('name', name);
        window.sessionStorage.setItem('username', username);
        window.sessionStorage.setItem('password', password);
        window.sessionStorage.setItem('email', email);
    }

    static removeLocalStorage() {
        window.sessionStorage.removeItem('name');
        window.sessionStorage.removeItem('username');
        window.sessionStorage.removeItem('password');
        window.sessionStorage.removeItem('email');
    }

    crtajFormu(host) {
        
        while (host.firstChild !== null) {
            host.removeChild(host.lastChild);
        }

        let divProfile = document.createElement('div');
        divProfile.className = 'profileContainer';
        host.appendChild(divProfile);

        let labela = null;
        let stavka = null;
        let listLabels = ["Name: ", "Username: ", "Email: ", "Password: "];
        let listTypes = ["text", "text", "text", "text"];
        let listClasses = ["nameInput", "usernameInput", "emailInput", "passwordInput"];
        let objectProperties = [this.name, this.username, this.email, this.password];
        let listOfInputs = [];//0 - name, 1 - username, 2 - email, 3 - password
        listLabels.forEach((item, index) => {
            labela = document.createElement("label");
            labela.innerHTML = item;
            host.appendChild(labela);

            stavka = document.createElement("input");
            stavka.type = listTypes[index];
            stavka.className = listClasses[index];
            stavka.disabled = true;
            listOfInputs.push(stavka);
            stavka.value = objectProperties[index];
            host.appendChild(stavka);
        })
        

        let tagsContainer = document.createElement('div');
        tagsContainer.classList.add('displayFlexColumn');
        let row;
        
        host.appendChild(tagsContainer);
        
        fetch("https://localhost:7012/Tag/GetAllTags").then(request => {
            if (request.ok) {
                request.json().then(data => {
                    data.forEach((element, index) => {
                        //crtaj dugme za svaki tag, checked oni koji ima u user.tags                    
                        if (index % 15 === 0) {
                            row = document.createElement("div");
                            row.classList.add('divItemRow');
                            tagsContainer.appendChild(row);
                        }
                        var currentTag = new Tag(element.genre)
                        currentTag.createTagButton(row, this.tags)
                        
                    })
                })
            }
        })

        let updateButton = document.createElement('button');
        updateButton.innerHTML = "Edit";
        updateButton.className = "updateUser";
        updateButton.onclick = (event) => {
            if (updateButton.innerHTML === "Edit") {
                listOfInputs.forEach((element) => {
                    element.disabled = false;
                })
                updateButton.innerHTML = 'Submit edits';
            }
            else {
                //sad ovde treba fetch i ostale stvari, treba mi i kontorller za update profile
                fetch("https://localhost:7012/User/UpdateUser/" + sessionStorage.getItem('username'), {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        name: listOfInputs[0].value,
                        username: listOfInputs[1].value,
                        email: listOfInputs[2].value,
                        password: listOfInputs[3].value,
                        tags: this.tags
                    })
                }).then(response => {
                    if (response.ok) {
                        listOfInputs.forEach((element) => {
                            element.disabled = true;
                        })
                        updateButton.innerHTML = 'Edit';
                        response.json().then(data => {
                            sessionStorage.setItem('username', data.username)
                            sessionStorage.setItem('name', data.name)
                            sessionStorage.setItem('email', data.email)
                            sessionStorage.setItem('password', data.password)
                        })
                    }
                    else {
                        alert ("Error happened, try again!")
                    }
                });
                
            }
            

        }
        host.appendChild(updateButton);

        let dugme = document.createElement('button');
        dugme.innerHTML = "Log out";
        dugme.className = "logOut";
        dugme.onclick = (event) => {
            User.removeLocalStorage();
            User.drawLoginForm(host);
            let friendsContainer = host.parentElement.querySelector(".friendsContainer")
            while (friendsContainer.firstChild) {
                friendsContainer.removeChild(friendsContainer.lastChild);
            }
            
        }
        host.appendChild(dugme);
        let friendsContainer = host.parentElement.querySelector(".friendsContainer")
        this.crtajSidebar(friendsContainer);

    }
    crtajSidebar(host) {
        while(host.firstChild) {
            host.removeChild(host.lastChild)
        }
        fetch("https://localhost:7012/User/GetFriends/" + this.username).then(response => {
                    if(response.ok) {
                        response.json().then(data => {
                            data.forEach((element, index) => {
                                this.friends.push(new User(element.name, element.username, null, element.email));
                                this.friends[index].drawFriend(host);
                            })
                        })
                    }
                    else
                    {
                        alert("Could not load any friends");
                    }
        })
    }

    drawFriend(host) {
        (() => {
            let friendElement = document.createElement("div");
            friendElement.className = "friendElement";

            friendElement.innerHTML = this.name;
            host.appendChild(friendElement);
            friendElement.onclick = (event) => {
                this.drawUserForm()
            }
        })();
    }

    drawUserForm() {
        //treba da se vidi user info, friends i reviews
        let mainContainer = document.querySelector(".mainContainer")
        while(mainContainer.firstChild) {
            mainContainer.removeChild(mainContainer.lastChild)
        }

        let userForm = document.createElement('div')
        userForm.className = 'userForm'
        mainContainer.appendChild(userForm)

        let mainHeader = document.createElement('div')
        mainHeader.className = 'userFormHeader'
        userForm.appendChild(mainHeader)
        let subHeader = document.createElement('div')
        subHeader.className = 'userFormHeader'
        userForm.appendChild(subHeader)

        let nameLabel = document.createElement('label')
        nameLabel.className = 'pageTitleLabel'
        nameLabel.textContent = "Name: " + this.name
        mainHeader.appendChild(nameLabel)

        //u main header bih hteo da stavim i dugme za dodavanje prijatelja, ali to cu kasnije

        let usernameLabel = document.createElement('label')
        usernameLabel.className = 'userFormSubheadElement'
        usernameLabel.textContent = "Username: " + this.username

        let emailLabel = document.createElement('label')
        emailLabel.className = 'userFormSubheadElement'
        emailLabel.textContent = "Email: " + this.email

        subHeader.appendChild(usernameLabel)
        subHeader.appendChild(emailLabel)

        let userFormBody = document.createElement('div')
        userFormBody.className = 'userFormBody'
        userForm.appendChild(userFormBody)

        let userFormFriends = document.createElement('div')
        userFormFriends.className = 'userFormFriends'
        userFormBody.appendChild(userFormFriends)

        //populate friends element
        this.crtajSidebar(userFormFriends)

        let addFriendButton = document.createElement('button')
        addFriendButton.className = 'addFriendButton'
        addFriendButton.textContent = ""
        mainHeader.appendChild(addFriendButton)

        fetch("https://localhost:7012/User/AreWeFriends/" + sessionStorage.getItem('username') + "/" + this.username).then(request => {
            if (request.ok) {
                addFriendButton.textContent = "Remove friend"
            }
            else {
                addFriendButton.textContent = "Add friend"
            }
        })
        addFriendButton.onclick = () => {
            if (addFriendButton.textContent == "Add friend") {
                fetch("https://localhost:7012/User/AddFriend/" + sessionStorage.getItem('username') + "/" + this.username, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "*/*"
                    }
                }).then(p => {
                    if(p.ok) {
                        alert("You are friends with " + this.name + " now!")
                        addFriendButton.textContent = 'Remove friend'
                    }
                    else {
                        alert("Problem adding friend, something is wrong!")
                    }
                })
            }
            else {
                fetch("https://localhost:7012/User/RemoveFriend/" + sessionStorage.getItem('username') + "/" + this.username, {
                    method: "DELETE",
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "*/*"
                    }
                }).then(p => {
                    if(p.ok) {
                        alert("You are no longer friends with " + this.name + "!")
                        addFriendButton.textContent = 'Add friend'
                    }
                    else {
                        alert("Problem removing friend, something is wrong!")
                    }
                })
            }
            
        }

        let userFormReviews = document.createElement('div')
        userFormReviews.className = 'userFormReviews'
        userFormBody.appendChild(userFormReviews)

        let userFormReviewLabel = document.createElement('label')
        userFormReviewLabel.textContent = "Reviews:"
        userFormReviews.appendChild(userFormReviewLabel)

        fetch("https://localhost:7012/Review/GetMyReviews/" + this.username).then(p => {
            p.json().then(data => {
                data.forEach(element => {
                    let item = new Rated(new User(element.user.name, element.user.username, element.user.password, element.user.email), new Comic(element.comic.comicId, element.comic.title, element.comic.description, element.comic.rating, element.comic.type), element.stars, element.comment, element.dateTime);
                    item.drawReviewWithComic(userFormReviews);
                });
            });
        });
    }

}
