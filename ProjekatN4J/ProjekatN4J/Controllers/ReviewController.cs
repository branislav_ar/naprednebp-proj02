﻿using Microsoft.AspNetCore.Mvc;
using Neo4jClient;
using ProjekatN4J.DomainModel;

namespace ProjekatN4J.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ReviewController : ControllerBase
    {
        GraphClient graphClient;

        public ReviewController()
        {
            graphClient = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "jasamja1");
            graphClient.Connect();
        }

        [Route("GetComicReviews/{comicId}")]
        [HttpGet]
        public IActionResult Index([FromRoute] int comicId)
        {
            var query = graphClient.Cypher
                .Match("(c:Comic {comicId: " + comicId + "})<-[r:RATED]-(u:User)")
                .Return((c, r, u) => new
                {
                    comic = c.As<Comic>(),
                    rating = r.As<Rated>(),
                    user = u.As<User>()
                })
                .Limit(20);
            var results = query.Results.ToList();
            List<Rated> reviews = new List<Rated>();
            foreach(var item in results)
            {
                reviews.Add(new Rated(new User(item.user.name, item.user.username, "", item.user.password), new Comic(item.comic.comicId, item.comic.title, item.comic.description, item.comic.type, item.comic.rating), item.rating.stars, item.rating.comment, item.rating.dateTime));
            }
            return Ok(reviews);
        }

        [Route("GetFriendActivity/{username}")]
        [HttpGet]
        public IActionResult GetFriendActivity([FromRoute] string username)
        {
            //treba da vratim u hronoloskom redosledu rating-e (latest first)
            var query = graphClient.Cypher
                .Match("(u:User {username: '" + username + "'})-[fr:FRIEND]->(n:User)-[r:RATED]->(c:Comic)")
                .Return((c, r, n) => new
                {
                    comic = c.As<Comic>(),
                    rating = r.As<Rated>(),
                    user = n.As<User>(),
                    date = r.As<Rated>().dateTime
                }).OrderByDescending("date")
                .Limit(100);
            var results = query.Results.ToList();
            List<Rated> reviews = new List<Rated>();
            foreach (var item in results)
            {
                reviews.Add(new Rated(new User(item.user.name, item.user.username, "", item.user.password), new Comic(item.comic.comicId, item.comic.title, item.comic.description, item.comic.type, item.comic.rating), item.rating.stars, item.rating.comment, item.rating.dateTime));
            }
            return Ok(reviews);
        }

        [Route("GetMyReviews/{username}")]
        [HttpGet]
        public IActionResult GetMyReviews([FromRoute] string username)
        {
            //treba da vratim u hronoloskom redosledu rating-e (latest first)
            var query = graphClient.Cypher
                .Match("(u:User {username: '" + username + "'})-[r:RATED]->(c:Comic)")
                .Return((c, r, u) => new
                {
                    comic = c.As<Comic>(),
                    rating = r.As<Rated>(),
                    user = u.As<User>(),
                    date = r.As<Rated>().dateTime
                }).OrderByDescending("date")
                .Limit(100);
            var results = query.Results.ToList();
            List<Rated> reviews = new List<Rated>();
            foreach (var item in results)
            {
                reviews.Add(new Rated(new User(item.user.name, item.user.username, "", item.user.password), new Comic(item.comic.comicId, item.comic.title, item.comic.description, item.comic.type, item.comic.rating), item.rating.stars, item.rating.comment, item.rating.dateTime));
            }
            return Ok(reviews);
        }

        [Route("AddReview")]
        [HttpPost]
        public IActionResult AddReview([FromBody]Rated rated)
        {
            rated.dateTime = DateTime.Now;
            var query = graphClient.Cypher
                .Match("(u:User),(c:Comic)")
                .Where(String.Format("u.username = '{0}' and c.comicId = {1}", rated.user.username, rated.comic.comicId))
                .Merge("(u)-[r:RATED {stars: " + rated.stars.ToString() + ", comment: '" + rated.comment + "', dateTime:'" + rated.dateTime + "'}]->(c)")
                .Return((r) => new
                {
                    relationship = r.As<Rated>(),
                }).Limit(1);
            var result = query.Results.FirstOrDefault();
            var getRatings = graphClient.Cypher
                .Match("(c:Comic {comicId: "+ rated.comic.comicId+"})<-[r:RATED]-(u:User)")
                .Return((r) => new 
                {   
                    stars = r.As<Rated>().stars
                })
                .Limit(1000);
            var ratings = getRatings.Results.ToList();
            double sum = 0;
            foreach(var item in ratings)
            {
                sum += item.stars;
            }
            double average = sum / (double)ratings.Count;
            var updateRatingQuery = graphClient.Cypher
                .Match("(c:Comic {comicId: " + rated.comic.comicId + "})")
                .Set(String.Format("c.rating = {0}", average));
            updateRatingQuery.ExecuteWithoutResults();
            Rated rated1;
            if (result != null)
            {
                rated1 = new(rated.user, rated.comic, result.relationship.stars, result.relationship.comment, result.relationship.dateTime);
                return Ok(rated1);
            }
            else
                return BadRequest();
        }


        [Route("UpdateReview")]
        [HttpPut]
        public IActionResult UpdateReveiw([FromBody] Rated rated)
        {
            try
            {
                rated.dateTime = DateTime.Now;
                var query = graphClient.Cypher
                    .Match("(u:User {username: '" + rated.user.username + "'})-[r:RATED]->(c:Comic {comicId:" + rated.comic.comicId + "})")
                    .Set("r = {comment: '" + rated.comment + "' , stars: " + rated.stars + ", dateTime:'" + rated.dateTime + "'}");
                query.ExecuteWithoutResults();

                var getRatings = graphClient.Cypher
                .Match("(c:Comic {comicId: " + rated.comic.comicId + "})<-[r:RATED]-(u:User)")
                .Return((r) => new
                {
                    stars = r.As<Rated>().stars
                })
                .Limit(1000);
                var ratings = getRatings.Results.ToList();
                double sum = 0;
                foreach (var item in ratings)
                {
                    sum += item.stars;
                }
                double average = sum / (double)ratings.Count;
                var updateRatingQuery = graphClient.Cypher
                    .Match("(c:Comic {comicId: " + rated.comic.comicId + "})")
                    .Set(String.Format("c.rating = {0}", average));
                updateRatingQuery.ExecuteWithoutResults();

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

        }

        [Route("DeleteReview/{username}/{comicId}")]
        [HttpDelete]
        public IActionResult DeleteReview([FromRoute] string username, [FromRoute] int comicId)
        {
            try
            {
                var query = graphClient.Cypher
                    .Match("(u:User {username: '" + username + "'})-[r:RATED]->(c:Comic {comicId:" + comicId + "})")
                    .Delete("r");
                query.ExecuteWithoutResults();

                var getRatings = graphClient.Cypher
                .Match("(c:Comic {comicId: " + comicId + "})<-[r:RATED]-(u:User)")
                .Return((r) => new
                {
                    stars = r.As<Rated>().stars
                })
                .Limit(1000);
                var ratings = getRatings.Results.ToList();
                double sum = 0;
                foreach (var item in ratings)
                {
                    sum += item.stars;
                }
                double average = sum / (double)ratings.Count;
                var updateRatingQuery = graphClient.Cypher
                    .Match("(c:Comic {comicId: " + comicId + "})")
                    .Set(String.Format("c.rating = {0}", average));
                updateRatingQuery.ExecuteWithoutResults();


                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}
