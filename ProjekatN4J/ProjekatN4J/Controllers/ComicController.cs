﻿using Microsoft.AspNetCore.Mvc;
using ProjekatN4J.DomainModel;
using Neo4jClient;
using Neo4jClient.Cypher;
using System.Globalization;

namespace ProjekatN4J.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ComicController : ControllerBase
    {
        GraphClient graphClient;


        public ComicController()
        {
            graphClient = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "jasamja1");
            graphClient.Connect();
        }

        [Route("SimilarToComic/{comicId}")]
        [HttpGet] 
        public IActionResult GetComics(int comicId)
        {
            var query = graphClient.Cypher
                .Match("(c:Comic {comicId: " + comicId + "})-[:BELONGS]->(t:Tag)<-[:BELONGS]-(rec:Comic)")
                .Where("c <> rec")

                .Return((rec) => new
                {
                    comic = rec.As<Comic>(),
                    frequency = rec.Count()
                })
                .OrderByDescending("frequency")
                .Limit(5);
            var data = query.Results.ToList();
            List<Comic> list = new List<Comic>();
            foreach (var item in data)
            {
                list.Add(new Comic(item.comic.comicId, item.comic.title, item.comic.description, item.comic.type, item.comic.rating));
            }
            return Ok(list);
        }

        [Route("GetRecommendationsByYourComics/{username}")]
        [HttpGet]
        public IActionResult GetRecByYourComic([FromRoute] string username)
        {
            var query = graphClient.Cypher
                .Match("(u:User {username: '"+username+"'})-[:RATED]->(c:Comic)-[:BELONGS]->(t:Tag)<-[:BELONGS]-(comic:Comic)")
                .Where("comic <> c")
                .Return((comic) => new
                {
                    comic = comic.As<Comic>(),
                    frequency = comic.Count()
                })
                .OrderByDescending("frequency")
                .Limit(3);
            var data = query.Results.ToList();
            List<Comic> list = new List<Comic>();
            foreach (var item in data)
            {
                list.Add(new Comic(item.comic.comicId, item.comic.title, item.comic.description, item.comic.type, item.comic.rating));
            }
            return Ok(list);
        }

        [Route("GetRecommendationsByFriends/{username}")]
        [HttpGet]
        public IActionResult GetByFriends([FromRoute] string username)
        {
            var query = graphClient.Cypher
                .Match("(u:User {username: '" + username + "'})-[:FRIEND]->(friend:User)-[:RATED]->(comic:Comic)")
                .Where("not (u)-[:RATED]->(comic)")
                .Return((comic) => new
                {
                    comic = comic.As<Comic>(),
                    frequency = comic.Count()
                })
                .OrderByDescending("frequency")
                .Limit(3);
            var data = query.Results.ToList();
            List<Comic> list = new List<Comic>();
            foreach (var item in data)
            {
                list.Add(new Comic(item.comic.comicId, item.comic.title, item.comic.description, item.comic.type, item.comic.rating));
            }
            return Ok(list);
        }

        [Route("GetRecommendationsBySelectedTags/{username}")]
        [HttpGet]
        public IActionResult GetBySelTags([FromRoute] string username)
        {
            var query = graphClient.Cypher
                .Match("(u:User {username: '" + username + "'})-[:PREFERS]->(tag:Tag)<-[:BELONGS]-(comic:Comic)")
                .Where("not (u)-[:RATED]->(comic)")
                .Return((comic) => new
                {
                    comic = comic.As<Comic>(),
                    frequency = comic.Count()
                })
                .OrderByDescending("frequency")
                .Limit(3);
            var data = query.Results.ToList();
            List<Comic> list = new List<Comic>();
            foreach (var item in data)
            {
                list.Add(new Comic(item.comic.comicId, item.comic.title, item.comic.description, item.comic.type, item.comic.rating));
            }
            return Ok(list);
        }

        [Route("GetPopularComics")]
        [HttpGet]
        public IActionResult GetPopularComics()
        {
            var query = graphClient.Cypher
                .Match("(c:Comic)<-[r:RATED]-(u:User)")
                .Return((c, u) => new
                {
                    comic = c.As<Comic>(),
                    frequency = u.Count(),
                    rating = c.As<Comic>().rating
                })
                .OrderByDescending("frequency*rating")
                .Limit(5);
            var result = query.Results.ToList();
            List<Comic> comics = new List<Comic>();
            foreach(var item in result)
            {
                comics.Add(new Comic(item.comic.comicId, item.comic.title, item.comic.description, item.comic.type, item.comic.rating));
            }
            return Ok(comics);
        }

        [Route("GetMyComics/{username}")]
        [HttpGet]
        public IActionResult GetMyComicsH([FromRoute] string username)
        {
            try
            {
                var query = graphClient.Cypher
                    .Match("(n:User)-[r:RATED]->(c:Comic)")
                    .Where(String.Format("n.username='{0}'", username))
                    .Return((c) => new
                    {
                        comic = c.As<Comic>()
                    })
                    .Limit(20);
                var result = query.Results.ToList();
                List<Comic> comics = new List<Comic>();
                foreach(var item in result)
                {
                    comics.Add(new Comic(item.comic.comicId, item.comic.title, item.comic.description, item.comic.type, item.comic.rating));
                }
                
                return Ok(comics);
            }
            catch (Exception ex)
            {
                return BadRequest("Error loading comics!");
            }
        }

        [Route("Search/{title}")]
        [HttpPost]
        public IActionResult Search([FromRoute] string title, [FromBody] string tags)
        {
            string[] titleParts = title.Split('+');

            string[] tagArray = tags.Split(" ");
            bool skipFirst = true;
            string whereTags = "(t.genre = '" + tagArray[0] + "'";
            foreach (string tag in tagArray)
            {
                if (skipFirst)
                {
                    skipFirst = false;
                    continue;
                }
                whereTags += " or t.genre = '" + tag + "'";
            }
            whereTags += ") or n.title =~ '(?i).*";
            foreach(string titlePart in titleParts)
            {
                whereTags += titlePart + ".*";
            }
            whereTags += "'";

            var query = graphClient.Cypher
                .Match("(n:Comic)-[b:BELONGS]->(t:Tag)")
                .Where(whereTags)
                .Return((n) => new
                {
                    comic = n.As<Comic>(),
                    frequency = n.Count()
                }).OrderBy("frequency").Limit(15);

            var result = query.Results.ToList();
            List<Comic> comics = new List<Comic>();
            foreach (var item in result)
            {
                comics.Add(new Comic(item.comic.comicId, item.comic.title, item.comic.description, item.comic.type, item.comic.rating));
            }

            return Ok(comics);
        }

        [Route("AddComic")]
        [HttpPost]
        public IActionResult AddComic([FromBody] Comic comic)
        {
            var query = graphClient.Cypher
                .Match("(c:Comic)")
                .Return((c) => new
                {
                    comic = c.As<Comic>()
                }).OrderByDescending("c.comicId")
                .Limit(1);
            var result = query.Results.FirstOrDefault();

            int comicId;
            if (result == null)
            {
                comicId = 0;
            }
            else
            {
                comicId = result.comic.comicId + 1;
            }

            var addComicQuery = graphClient.Cypher
                .Merge("(c:Comic {title: '"+comic.title+"', description: '"+comic.description+"', rating: 0, type: '"+comic.type+"', comicId: "+ comicId +"})")
                .Return((c) => new
                {
                    comic = c.As<Comic>()
                }).Limit(1);
            var results = addComicQuery.Results.FirstOrDefault();
            return Ok(results.comic);
        }

        [Route("DeleteComic/{comicId}")]
        [HttpDelete]
        public IActionResult DeleteComic([FromRoute] int comicId)
        {
            graphClient.Cypher.Match("(c:Comic {comicId: " + comicId + "})").DetachDelete("c").ExecuteWithoutResults();
            return Ok();
        }

        [Route("EditComic")]
        [HttpPut]
        public IActionResult EditComic([FromBody] Comic comic)
        {
            try
            {
                var result = graphClient.Cypher
                    .Match("(c:Comic {comicId: " + comic.comicId + "})")
                    .Set("c = {title: '" + comic.title + "', description: '" + comic.description + "', type: '" + comic.type + "'}")
                    .Return((c) => new
                    {
                        comic = c.As<Comic>()
                    }).Results.FirstOrDefault();
                if (result == null)
                    throw new Exception();
                return Ok(result.comic);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}
