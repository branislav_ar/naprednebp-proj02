﻿using Microsoft.AspNetCore.Mvc;
using Neo4jClient;
using Neo4jClient.Cypher;
using ProjekatN4J.DomainModel;

namespace ProjekatN4J.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TagController : ControllerBase
    {
        GraphClient graphClient;

        public TagController()
        {
            graphClient = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "jasamja1");
            graphClient.Connect();
        }


        [Route("GetAllTags")]
        [HttpGet]
        public IActionResult AllTags()
        {
            var query = graphClient.Cypher.Match("(n:Tag)").Return((t) => Return.As<string>("n.genre")).Limit(100);
            List<string> dataTags = query.Results.ToList<string>();
            List<Tag> tags = new List<Tag>();
            foreach (var item in dataTags)
            {
                tags.Add(new Tag(item));
            }
            return Ok(tags);
        }

        [Route("AddTag")]
        [HttpPost]
        public IActionResult AddTag([FromBody] Tag tag)
        {
            var query = graphClient.Cypher.Match("(n:Tag)").Where(String.Format("n.genre=~'(?i){0}'", tag.genre))
                .Return((n) => Return.As<string>("n.genre")).Limit(1);

            var returnedTag = query.Results.ToList();
            Tag tagReturned;
            if (returnedTag.Count < 1)
            {
                var queryAddNew = graphClient.Cypher.Create("(n:Tag {genre:'" + tag.genre + "'})").Return((n) => Return.As<string>("n.genre")).Limit(1);
                returnedTag = queryAddNew.Results.ToList();
                tagReturned = new Tag(returnedTag[0]);
            }
            tagReturned = new Tag(returnedTag[0]);

            return Ok(returnedTag);
        }

        [Route("GetUserTags/{username}")]
        [HttpGet]
        public IActionResult GetUserTags([FromRoute] string username)
        {
            var query = graphClient.Cypher.Match("((u:User)-[r:PREFERS]->(t:Tag))")
                .Where(String.Format("u.username = '{0}'", username))
                .Return((t) => Return.As<string>("t.genre")).Limit(10);
            var data = query.Results.ToList();
            List<Tag> tags = new List<Tag>();
            foreach (var item in data)
            {
                tags.Add(new Tag(item));
            }
            return Ok(tags);
        }

        [Route("GetComicTags")]
        [HttpPost]
        public IActionResult GetComicTags([FromBody] string comicTitle)
        {
            var query = graphClient.Cypher.Match("(c:Comic)-[r:BELONGS]->(t:Tag)")
                .Where(String.Format("c.title='{0}'", comicTitle))
                .Return((t) => Return.As<string>("t.genre")).Limit(100);
            var data = query.Results.ToList();
            List<Tag> tags = new List<Tag>();
            foreach (var item in data)
            {
                tags.Add(new Tag(item));
            }
            return Ok(tags);
        }
    }
}
