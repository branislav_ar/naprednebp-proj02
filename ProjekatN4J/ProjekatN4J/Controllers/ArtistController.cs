﻿using Microsoft.AspNetCore.Mvc;
using Neo4jClient;
using Neo4jClient.Cypher;
using ProjekatN4J.DomainModel;

namespace ProjekatN4J.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ArtistController : ControllerBase
    {
        GraphClient graphClient;
        public ArtistController()
        {
            graphClient = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "jasamja1");
            graphClient.Connect();
        }

        [Route("GetArtist/{comicId}")]
        [HttpGet]
        public IActionResult getartist([FromRoute] int comicId)
        {
            var query = graphClient.Cypher
                .Match("(a:Artist)-[:WROTE]->(c:Comic {comicId: " + comicId + "})")
                .Return((a) => new
                {
                    artist = a.As<Artist>()
                })
                .Limit(1);
            var result = query.Results.FirstOrDefault();
            Artist artist = new Artist(result.artist.name, result.artist.bio);
            return Ok(artist);
        }

    }
}
