﻿using Microsoft.AspNetCore.Mvc;
using Neo4jClient;
using Neo4jClient.Cypher;
using ProjekatN4J.DomainModel;

namespace ProjekatN4J.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {

        GraphClient graphClient;

        public UserController()
        {
            graphClient = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "jasamja1");
            graphClient.Connect();
        }

        [Route("Login/{username}")]
        [HttpPost]
        public IActionResult Function(string username, [FromBody] string password)
        {
            try
            {
                var queryUser = graphClient.Cypher
                    .Match("(n:User)")
                    .Where(String.Format("n.username = '{0}' and n.password = '{1}'", username, password))
                    .Return((n) => new
                    {
                        user = n.As<User>()
                    }).Limit(1);
                var data = queryUser.Results.ToList();
                User user = new User(data[0].user.name, data[0].user.username, data[0].user.password, data[0].user.email);
                //treba da pokupim i tagove koje je follow-ovao
                var queryTags = graphClient.Cypher
                    .Match("(n:User)-[p:PREFERS]->(t:Tag)")
                    .Where(String.Format("n.username = '{0}'", username))
                    .Return((t) => Return.As<string>("t.genre")).Limit(10);
                var dataTags = queryTags.Results.ToList();
                foreach (var item in dataTags)
                {
                    user.tags.Add(new Tag(item));
                }

                return Ok(user);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [Route("Register")]
        [HttpPost]
        public IActionResult Register([FromBody] User korisnik)
        {
            string error = "";

            var query = graphClient.Cypher
                .Match("(n:User)")
                .Where(String.Format("n.username = '{0}' or n.email = '{1}'", korisnik.username, korisnik.email))
                .Return((n) => new
                {
                    user = n.As<User>()
                })
                .Limit(2);
            var data = query.Results.ToList();
            List<User> users = new List<User>();
            foreach (var item in data)
            {
                users.Add(new User(item.user.name, item.user.username, item.user.password, item.user.email));
            }
            if (users.Count > 0)
            {
                foreach (User user in users)
                {
                    if (user.username == korisnik.username)
                        error += String.Format("Vec postoji korisnik sa ovim korisnickim imenom: {0}\n", user.username);
                    if (user.email == korisnik.email)
                        error += String.Format("Vec postoji korisnik sa ovim e-mailom: {0}\n", user.email);
                }
                return BadRequest(error);
            }
            var queryAddProfile = graphClient.Cypher
                .Create("(n:User {name:'" + korisnik.name + "', username:'" + korisnik.username + "', password:'" + korisnik.password + "', email:'" + korisnik.email + "'})")
                .Return((n) => new
                {
                    returned = n.As<User>()
                })
                .Limit(1);
            var list = queryAddProfile.Results.ToList();

            foreach (Tag tag in korisnik.tags)
            {
                var addPrefersRelation = graphClient.Cypher
                    .Match("(n:User), (t:Tag)")
                    .Where(String.Format("n.username = '{0}' and t.genre = '{1}'", korisnik.username, tag.genre))
                    .Create("(n)-[r:PREFERS]->(t)")
                    .Return((t) => Return.As<string>("t.genre"));
                var result = addPrefersRelation.Results.ToList();

            }
            return Ok(korisnik);

        }

        [Route("GetFriends/{username}")]
        [HttpGet]
        public IActionResult GetFriends([FromRoute] string username)
        {
            var query = graphClient.Cypher
                .Match("(u:User)-[f:FRIEND]->(n:User)")
                .Where(String.Format("u.username = '{0}'", username))
                .Return((n) => new
                {
                    user = n.As<User>()
                })
                .Limit(20);

            var results = query.Results.ToList();
            List<User> friends = new List<User>();
            foreach (var result in results)
            {
                friends.Add(new User(result.user.name, result.user.username, "", result.user.email));
            }
            return Ok(friends);
        }

        [Route("UpdateUser/{oldUsername}")]
        [HttpPost]
        public IActionResult UpdateUser([FromBody] User user, [FromRoute] string oldUsername)
        {
            //znam da sigurno postoji, jer ga uzimam sa login tab-a
            var queryDeleteTags = graphClient.Cypher
                .Match("(u:User)-[r:PREFERS]->(t:Tag)")
                .Where(String.Format("u.username = '{0}'", oldUsername))
                .Delete("r");
            
            queryDeleteTags.ExecuteWithoutResults();
            //TODO: update-uj informacije za korisnika sa oldUsername
            var queryUpdateInfo = graphClient.Cypher
                .Match("(u:User)")
                .Where(String.Format("u.username = '{0}'", oldUsername))
                .Set("u = {username: '" + user.username + "', name: '" + user.name + "', email: '" + user.email + "', password: '" + user.password + "'}")
                .Return((u) => new
                {
                    user = u.As<User>()
                }).Limit(1);
            var resultUpdate = queryUpdateInfo.Results.FirstOrDefault();

            User updatedUser = new User(resultUpdate.user.name, resultUpdate.user.username, resultUpdate.user.password, resultUpdate.user.email);

            //TODO: dodaj veze sa tagovima
            bool skipFirst = true;
            string whereTags = "(t.genre = '" + user.tags[0].genre + "'";
            foreach (Tag tag in user.tags)
            {
                if (skipFirst)
                {
                    skipFirst = false;
                    continue;
                }
                whereTags += " or t.genre = '" + tag.genre + "'";
            }
            whereTags += ") and u.username = '" + user.username + "'";
            var updateTags = graphClient.Cypher
                .Match("(u:User),(t:Tag)")
                .Where(whereTags)
                .Create("(u)-[r:PREFERS]->(t)")
                .Return((t) => new
                {
                    tag = t.As<Tag>()
                }).Limit(100);
            var resultUpdateTags = updateTags.Results.ToList();
            foreach(var item in resultUpdateTags)
            {
                updatedUser.tags.Add(new Tag(item.tag.genre));
            }

            return Ok(updatedUser);
        }


        [Route("AddFriend/{usernameOne}/{usernameTwo}")]
        [HttpPost]
        public IActionResult AddFriend([FromRoute] string usernameOne, [FromRoute] string usernameTwo)
        {
            try
            {
                if (string.IsNullOrEmpty(usernameOne) || string.IsNullOrEmpty(usernameTwo))
                    throw new Exception();
                var query = graphClient.Cypher
                    .Match("(n:User), (u:User)")
                    .Where(String.Format("n.username = '{0}' and u.username = '{1}'", usernameOne, usernameTwo))
                    .Merge("(n)-[r:FRIEND]->(u)")
                    .Return((n, u) => new {
                        friendOne = n.As<User>(),
                        friendTwo = u.As<User>()
                    }).Limit(1);
                var result = query.Results.ToList();

                if (usernameOne == result[0].friendOne.username && usernameTwo == result[0].friendTwo.username)
                    return Ok();
                else return BadRequest();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

        }

        [Route("RemoveFriend/{usernameOne}/{usernameTwo}")]
        [HttpDelete]
        public IActionResult RemoveFriend([FromRoute] string usernameOne, [FromRoute] string usernameTwo)
        {
            try
            {
                if (string.IsNullOrEmpty(usernameOne) || string.IsNullOrEmpty(usernameTwo))
                    throw new Exception();
                var query = graphClient.Cypher
                    .Match("(n:User {username: '"+usernameOne+"'})-[r:FRIEND]->(u:User {username: '"+usernameTwo+"'})")
                    .Delete("r");
                query.ExecuteWithoutResults();

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [Route("DeleteProfile/{username}")]
        [HttpDelete]
        public IActionResult DeleteProfile([FromRoute] string username)
        {
            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    throw new Exception();
                }
                //var queryDeleteTags = graphClient.Cypher
                //.Match("(u:User)-[r:PREFERS]->(t:Tag)")
                //.Where(String.Format("u.username = '{0}'", username))
                //.Delete("r");

                //var queryDeleteReviews = graphClient.Cypher
                //    .Match("(u:User)-[r:RATED]->(c:Comic)")
                //    .Where(String.Format("u.username = '{0}'", username))
                //    .Delete("r");


                var query = graphClient.Cypher
                    .Match("(u:User)")
                    .Where(String.Format("u.username = '{0}'", username))
                    .DetachDelete("u");

                query.ExecuteWithoutResults();
                return Ok();
                
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [Route("GetFriendRecommendations/{username}")]
        [HttpGet]
        public IActionResult GetFriendRecommendations([FromRoute] string username)
        {
            var result = graphClient.Cypher
                .Match("(u:User {username: '" + username + "'})-[fr:FRIEND]->(friend:User)-[r:FRIEND]->(n:User)")
                .Where("u <> n and not (u)-[:FRIEND]->(n)")
                .Return((n) => new
                {
                    friend = n.As<User>(),
                    count = n.Count()
                }).OrderByDescending("count").Limit(6).Results.ToList();

            List<User> listOfRecommendations = new List<User>();
            foreach(var item in result)
            {
                listOfRecommendations.Add(new ProjekatN4J.User(item.friend.name, item.friend.username, "", item.friend.email));
            }
            return Ok(listOfRecommendations);
        }

        [Route("AreWeFriends/{usernameOne}/{usernameTwo}")]
        [HttpGet]
        public IActionResult AreWeFriends([FromRoute] string usernameOne, [FromRoute] string usernameTwo)
        {
            var result = graphClient.Cypher
                .Match("(u:User {username: '" + usernameOne + "'})-[:FRIEND]->(n:User {username: '" + usernameTwo + "'})")
                .Return((u, n) => new
                {
                    friendOne = u.As<User>(),
                    friendTwo = n.As<User>(),
                }).Limit(1).Results.FirstOrDefault();
            if (result != null)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}