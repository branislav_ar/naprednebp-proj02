﻿namespace ProjekatN4J.DomainModel
{
    public class Rated
    {
        public User user { get; set; }
        public Comic comic { get; set; }

        public int stars { get; set; }

        public string comment { get; set; }

        public DateTime dateTime { get; set; }

        public Rated()
        {
            
        }
        public Rated(User kor, Comic com, int ocena, string comment, DateTime vreme)
        {
            this.user = kor;
            this.comic = com;
            this.stars = ocena;
            this.comment = comment;
            this.dateTime = vreme;
        }
    }
}
