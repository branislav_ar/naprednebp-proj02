﻿namespace ProjekatN4J.DomainModel
{
    public class Comic
    {

        public int comicId { get; set; }
        public string title { get; set; }

        public string description { get; set; }

        public double rating { get; set; }
        public string type { get; set; }

        public Comic(int id, string naslov, string opis, string tip, double rating)
        {
            this.comicId = id;
            this.title = naslov;
            this.description = opis;
            this.type = tip;
            this.rating = Math.Round(rating, 2);
        }

        public Comic() { }
    }
}
