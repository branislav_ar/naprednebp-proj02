﻿namespace ProjekatN4J.DomainModel
{
    public class Tag
    {
        public string genre { get; set; }

        public Tag(string genre)
        {
            this.genre = genre;
        }

        public Tag()
        {

        }
    }
}
