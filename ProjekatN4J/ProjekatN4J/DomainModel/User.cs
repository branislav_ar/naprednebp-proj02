﻿using ProjekatN4J.DomainModel;

namespace ProjekatN4J
{
    public class User
    {
        public string name { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }

        public List<Tag> tags { get; set; }

        public User(string ime, string korIme, string sifra, string mejl)
        {
            this.name = ime;
            this.username = korIme;
            this.password = sifra;
            this.email = mejl;
            this.tags = new List<Tag>();
        }

        public User() 
        {
            this.tags = new List<Tag>();
        }
    }
}
