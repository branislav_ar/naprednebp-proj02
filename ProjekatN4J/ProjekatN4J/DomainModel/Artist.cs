﻿namespace ProjekatN4J.DomainModel
{
    public class Artist
    {
        public String name { get; set; }

        public String bio { get; set; }

        public Artist(String ime, String bio)
        {
            
            this.name = ime;
            this.bio = bio;
        }

        public Artist()
        {

        }
    }
}
